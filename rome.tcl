 #
 # mdb@nectarine.info (c) 2004, www.insaneminds.org (c) 2004, All rights reserved.
 #
 # Tcl script for Eggdrop(www.egghelp.org)
 #
 # This tcl create new channel command !insulta that's make a italian roman insult
 #
 # Permission to link to this code from the internet is hereby granted.
 #

# Set Insult Database File
set insultfile scripts/insults.txt

# Set newinsult command log
set logfille scripts/insults.log

### DONT EDIT BELOW ###

bind pub - !insulta pub:insulta
bind pub - !newinsult pub:newinsult


proc pub:insulta { nick mask hand chan args } {
	global insultfile logfille
	set handlez [open $insultfile r]
	set arr ""
	# USAGE if ""
	while { ![eof $handlez] } {
	
		set lineH [gets $handlez]
		if { $lineH != "" } {
			set arr [linsert $arr end $lineH]
		}
	
	}

	close $handlez

	puthelp "PRIVMSG $chan : $args: [lindex $arr [rand [llength $arr]]]"
}


proc pub:newinsult { nick mask hand chan args } {
	global insultfile logfille
	#correzione IF
	if { $args == "" } {
		putnotc $nick "Usage: !newinsult insult"
		return 0

	}
	set handlez [open $insultfile a]
	# Funzione per pulire dalle { }
	puts $handlez $args
	close $handlez
	putnotc $nick "Your insult has been added!"
	set handleza [open $logfille a]
	puts $handleza "$nick ($mask) of $chan Added: $args"
	close $handleza
	return 1
}


